# lec-invoice-fastapi

FastAPI application for LEC-Invoice

- [lec-invoice-fastapi](#lec-invoice-fastapi)
  - [Note](#note)
    - [Heroku and Database](#heroku-and-database)
    - [Starting up database locally](#starting-up-database-locally)
    - [Test cases are a bit different](#test-cases-are-a-bit-different)
  - [docker-compose.yml](#docker-composeyml)
  - [ORM](#orm)
  - [migration](#migration)
    - [Step1: Initiate alembic(only once)](#step1-initiate-alembiconly-once)
    - [Step2: (Auto) Generate migration files](#step2-auto-generate-migration-files)
    - [Step3: Apply migrations](#step3-apply-migrations)
    - [NOTES](#notes)
      - [1. Setting metadata for multiple models](#1-setting-metadata-for-multiple-models)
  - [Convenience bash](#convenience-bash)
    - [reset.sh](#resetsh)
    - [run_test.sh](#run_testsh)
    - [show_log.sh](#show_logsh)
    - [initial.sh](#initialsh)
    - [db.sh](#dbsh)
    - [makemigrations.sh](#makemigrationssh)
    - [migrate.sh](#migratesh)
  - [How to separate test database](#how-to-separate-test-database)
  - [TODO](#todo)

---

## Note

### Heroku and Database

If you want to upload FastAPI application to Heroku, instead of separating postgres info in docker-compose.yml -> services -> api.
Instead set a DATABASE_URL environment valiable and set a postgres database url.

### Starting up database locally

Before you even run `docker compose up -d`, you **MUST** fill in the following environment variables in both **api** and **db** section in `docker-compose.yml`.

- POSTGRES_USER
- POSTGRES_PASSWORD
- POSTGRES_DB

### Test cases are a bit different

This project's main purpose is to test out new things.
For this reason, test files have a slightly different way of running the code.
This is intended.

---

## docker-compose.yml

```yml
version: "3"

services:
    api:
        build:
            dockerfile: Dockerfile.dev
            context: .
        command: uvicorn app.main:app --reload --host 0.0.0.0 --port 8000
        environment:
            APPLICATION_NAME: lec-invoice
            ENVIRONMENT: development
            SECRET_KEY:
            DEBUG: 1
            API_VERSION: 1
            CORS_ORIGINS: "localhost"
            POSTGRES_USER:
            POSTGRES_PASSWORD:
            POSTGRES_DB:
        volumes:
            - .:/app
        ports:
            - 8000:8000
        depends_on:
            - db
    db:
        image: postgres:11
        ports:
            - "5432:5432"
        volumes:
            - postgres_data:/var/lib/postgresql/data/
        environment:
            POSTGRES_HOST_AUTH_METHOD: trust
            POSTGRES_USER:
            POSTGRES_PASSWORD:
            POSTGRES_DB:

volumes:
    postgres_data:
```

---

## ORM

For orm, we use **[ormar](https://github.com/collerek/ormar/)**.

---

## migration

When using ormar, we need to make migration files by ourselves.
For this we use **[alembic](https://github.com/alembic/alembic)**.

### Step1: Initiate alembic(only once)

```console
alembic init migrations
```

`migrations` is a name of the folder. This can be anything.

### Step2: (Auto) Generate migration files

```console
alembic revision --autogenerate -m "write migration comments"
```

### Step3: Apply migrations

```console
alembic upgrade head
```

### NOTES

#### 1. Setting metadata for multiple models

When you want to make a migration for multiple models, you need to **import** the models into `env.py` in addition to setting the metadata to `target_metadata`.

```py:env.py
from app.users.models import User
from app.venues.models import Venue
from app.configs.database import metadata

target_metadata = metadata
```

---

## Convenience bash

### reset.sh

stop and start docker-compose

```bash
docker compose down
docker compose up -d $1
```

### run_test.sh

run tests with docker-compose

```bash
if [ $# == 0 ]; then
    docker compose exec api pytest app/users -v --capture=no
    docker compose exec api pytest app/contacts -v --capture=no
    docker compose exec api pytest app/venues -v --capture=no
    docker compose exec api pytest app/invoices -v --capture=no
    docker compose exec api pytest app/contracts -v --capture=no
else
    docker compose exec api pytest $1 -v --capture=no
fi
```

### show_log.sh

show logs

```bash
clear
docker compose logs $1
```

### initial.sh

```bash
docker compose up -d --build
docker compose exec api pytest app/users -v --capture=no
docker compose exec api pytest app/contacts -v --capture=no
docker compose exec api pytest app/venues -v --capture=no
docker compose exec api pytest app/contracts -v --capture=no
docker compose exec api pytest app/invoices -v --capture=no
```

### db.sh

```bash
docker compose exec api alembic "$@"
```

### makemigrations.sh

```bash
docker compose exec api alembic revision --autogenerate -m "$@"
```

### migrate.sh

```bash
docker compose exec api alembic upgrade head
```

---

## How to separate test database

Just change the environment variable `ENVIRONMENT` to `test` from `development`.

---

## TODO

- [x] Separate test database from development and production database.
- [ ] Update model field `updated_at` when updating data.
