import random
import asyncio
from typing import Generator, List

import databases
from fastapi.testclient import TestClient
from passlib.hash import bcrypt
import pytest
import sqlalchemy

from app.users.models import User
from app.main import app
from app.configs.database import database, metadata, TEST_DB_URL
from app.configs.settings import settings
from app.resources.paths import Paths


@pytest.fixture(autouse=True, scope="session")
def create_test_database():
    engine = sqlalchemy.create_engine(TEST_DB_URL)
    metadata.drop_all(engine)
    metadata.create_all(engine)
    yield
    # metadata.drop_all(engine)


@pytest.fixture(scope="session")
def event_loop():
    """Force the pytest-asyncio loop to be the main one."""
    loop = asyncio.get_event_loop()
    yield loop


# NOTE(joshua-hashimoto): ormarでtest時にDBにアクセスするためのfixtureメソッド
@pytest.fixture(scope="session")
@pytest.mark.asyncio
async def db():
    async def wrapper(func):
        async with database:
            async with database.transaction(force_rollback=False):
                return await func
    return wrapper


@pytest.fixture(scope="session")
def client() -> Generator:
    yield TestClient(app)


async def get_rand_user() -> User:
    users: List[User] = await User.objects.filter(email__icontains="@example.com", is_active=True).all()
    if len(users) == 0:
        email = "example0@example.com"
        password = "Abcd1234"
        username = email[:email.index('@')]
        password_hash = bcrypt.hash(password)
        # create user
        user = await User.objects.create(
            username=username,
            email=email,
            password_hash=password_hash
        )
        return user
    return random.choice(users)


@pytest.fixture(scope="session")
@pytest.mark.asyncio
async def rand_user(db):
    return await db(get_rand_user())


@pytest.fixture(scope="session")
def login(client: TestClient):
    def wrapper(user):
        user_credentials = {
            'email': user.email,
            'password': 'Abcd1234',
        }
        url = Paths.auth.test_url + Paths.auth.login
        response = client.post(url, json=user_credentials)
        return response
    return wrapper


@pytest.fixture(scope="session")
def logout(client: TestClient):
    # delete user credentials from cookies
    def wrapper():
        return client.cookies.clear()
    return wrapper
