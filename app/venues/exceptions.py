from typing import Any, Dict, Optional

from fastapi import HTTPException, status


class VenueDoesNotExistException(HTTPException):

    def __init__(self, detail: Any = None, headers: Optional[Dict[str, Any]] = None) -> None:
        status_code = status.HTTP_400_BAD_REQUEST
        detail = "Venue does not exist."
        super().__init__(status_code, detail=detail, headers=headers)
