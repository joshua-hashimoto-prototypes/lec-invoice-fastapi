from typing import Optional

import ormar

from app.users.models import User
from app.resources.models import CoreModel


class Venue(CoreModel):
    user: Optional[User] = ormar.ForeignKey(User, related_name='venues')
    name: str = ormar.String(max_length=50)
    description: str = ormar.Text(nullable=True)

    class Meta(ormar.ModelMeta):
        tablename = "venues"
