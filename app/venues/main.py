from fastapi import FastAPI

from .routers import setup_venues_routers


def setup_venues(app: FastAPI):
    setup_venues_routers(app)
