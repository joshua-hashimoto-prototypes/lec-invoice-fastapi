from .models import Venue

VenueIn = Venue.get_pydantic(include={"name", "description"})
VenueOut = Venue.get_pydantic(include={"id", "name", "description"})
