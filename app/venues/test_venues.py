import random
import json

from fastapi import status
from fastapi.testclient import TestClient
import pytest

from .models import Venue
from .schemas import VenueOut
from app.resources.utils import random_string
from app.resources.paths import Paths


@pytest.fixture(scope="module")
def user(rand_user):
    return rand_user


@pytest.fixture
async def rand_venue(user, db):
    venues = await db(Venue.objects.filter(user=user, is_active=True).all())
    if len(venues) == 0:
        return await db(Venue.objects.create(user=user, name="filler venue", description=""))
    return random.choice(venues)


def test_api_can_add_venues(client: TestClient, login, logout, user):
    with client as client:
        login(user)
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        context = {
            'name': random_string(),
            'description': random_string(),
        }
        url = Paths.venue.test_url + Paths.venue.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_201_CREATED
        venue = json.loads(response.text)
        assert venue['id'] is not None
        assert venue['name'] == context['name']
        assert venue['description'] == context['description']
    logout()


def test_api_cannot_add_venue(client: TestClient):
    with client as client:
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None)
        }
        context = {
            'name': random_string(),
            'description': random_string(),
        }
        url = Paths.venue.test_url + Paths.venue.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_fetch_user_related_venues(client: TestClient, login, logout, user):
    with client as client:
        login(user)
        url = Paths.venue.test_url + Paths.venue.all
        response = client.get(url)
        assert response.status_code == status.HTTP_200_OK
    logout()


def test_api_cannot_fetch_user_related_venues(client: TestClient):
    with client as client:
        url = Paths.venue.test_url + Paths.venue.all
        response = client.get(url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_fetch_venue_detail(client: TestClient, login, logout, user, rand_venue):
    with client as client:
        login(user)
        url = Paths.venue.test_url + "/" + str(rand_venue.id)
        response = client.get(url)
        assert response.status_code == status.HTTP_200_OK
        response_data = json.loads(response.text)
        response_venue = VenueOut(**response_data)
        assert response_venue.id == rand_venue.id
    logout()


def test_api_cannot_fetch_venue_detail(client: TestClient, rand_venue):
    with client as client:
        url = Paths.venue.test_url + "/" + str(rand_venue.id)
        response = client.get(url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_edit_venue(client: TestClient, login, logout, user, rand_venue):
    # NOTE(joshua-hashimoto): withの中ではasyncの処理は実行できない
    with client as client:
        login(user)
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        context = {
            'name': 'editted name',
            'description': 'editted description',
        }
        url = Paths.venue.test_url + '/' + str(rand_venue.id) + Paths.venue.edit
        response = client.put(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_200_OK
        response_data = json.loads(response.text)
        response_venue = VenueOut(**response_data)
        assert response_venue.id == rand_venue.id
        assert response_venue.name == context['name']
        assert response_venue.description == context['description']
    logout()


def test_api_cannot_edit_venue(client: TestClient, rand_venue):
    # NOTE(joshua-hashimoto): withの中ではasyncの処理は実行できない
    with client as client:
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        context = {
            'name': 'editted name',
            'description': 'editted description',
        }
        url = Paths.venue.test_url + '/' + str(rand_venue.id) + Paths.venue.edit
        response = client.put(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    

@pytest.mark.asyncio
async def test_api_can_delete_venue(client: TestClient, db, login, logout, user, rand_venue):
    user_venues = await db(Venue.objects.filter(user=user, is_active=True).all())
    with client as client:
        login(user)
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        url = Paths.venue.test_url + "/" + str(rand_venue.id) + Paths.venue.delete
        response = client.delete(url, headers=headers)
        assert response.status_code == status.HTTP_204_NO_CONTENT
    updated_user_venues = await db(Venue.objects.filter(user=user, is_active=True).all())
    assert len(user_venues) != len(updated_user_venues)
    logout()
    

@pytest.mark.asyncio
async def test_api_cannot_delete_venue(client: TestClient, rand_venue):
    with client as client:
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        url = Paths.venue.test_url + "/" + str(rand_venue.id) + Paths.venue.delete
        response = client.delete(url, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


# def test_api_can_fetch_contracts_related_to_venue(client: TestClient, event_loop: asyncio.AbstractEventLoop):
#     user_login(client, event_loop)

#     async def get_venue():
#         venues = await Venue.filter(is_active=True)
#         return venues[0]

#     venue = event_loop.run_until_complete(get_venue())
#     uri = f'/{PREFIX_URL}/{venue.id}/contracts'
#     response = client.get(uri)
#     assert response.status_code == status.HTTP_200_OK
#     data = json.loads(response.text)
#     decoded_contracts = data.get('contracts', None)
#     assert isinstance(decoded_contracts, list)
#     user_logout(client)


# def test_api_cannot_fetch_contracts_related_to_venue(client: TestClient, event_loop: asyncio.AbstractEventLoop):
#     async def get_venue():
#         venues = await Venue.filter(is_active=True)
#         return venues[0]

#     venue = event_loop.run_until_complete(get_venue())
#     uri = f'/{PREFIX_URL}/{venue.id}/contracts'
#     response = client.get(uri)
#     assert response.status_code == status.HTTP_401_UNAUTHORIZED
