from typing import List
import uuid

from fastapi import FastAPI, APIRouter, Depends, Body, Path, status
from fastapi.logger import logger
from fastapi_jwt_auth import AuthJWT

from .exceptions import VenueDoesNotExistException
from .models import Venue
from .schemas import VenueIn, VenueOut
from app.users.services import get_user_from_db
from app.contracts.models import Contract
from app.contracts.schemas import ContractOut
from app.resources.constants import BASE_API_URL
from app.resources.exception_handlers import HttpException
from app.resources.paths import Paths


app_name = Paths.venue.base

router = APIRouter()


def setup_venues_routers(app: FastAPI):
    app.include_router(
        router,
        prefix=f'/{BASE_API_URL}/{app_name}',
        tags=[app_name],
    )


@router.get(Paths.venue.all, response_model=List[VenueOut])
async def get_all_venues(Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        venues = await Venue.objects.filter(user=user, is_active=True).all()
        return venues
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to fetch related data.', detail=str(err))


@router.post(Paths.venue.add, status_code=status.HTTP_201_CREATED, response_model=VenueOut)
async def create_venue(venue_in: VenueIn = Body(...),
                       Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        venue = await Venue.objects.create(user=user, **venue_in.dict())
        return venue
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to save data.', detail=str(err))


@router.get('/{venue_id}', response_model=VenueOut)
async def fetch_venue(venue_id: uuid.UUID = Path(...), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        if not (venue := await Venue.objects.get(id=venue_id, user=user)):
            raise VenueDoesNotExistException()
        return venue
    except Exception as err:
        logger.error(err)
        raise HttpException(message="failed to fetch data", detail=str(err))


@router.put('/{venue_id}' + Paths.venue.edit, response_model=VenueOut)
async def update_venue(venue_id: uuid.UUID = Path(...),
                       venue_in: VenueIn = Body(...),
                       Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        if not (venue := await Venue.objects.get(id=venue_id, user=user)):
            raise VenueDoesNotExistException()
        await venue.update(**venue_in.dict())
        return venue
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to update data.', detail=str(err))


@router.delete('/{venue_id}' + Paths.venue.delete, status_code=status.HTTP_204_NO_CONTENT)
async def delete_venue(venue_id: uuid.UUID = Path(...), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        if not (venue := await Venue.objects.get(id=venue_id, user=user)):
            raise VenueDoesNotExistException()
        await venue.update(is_active=False)
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to delete data.', detail=str(err))


# @router.get('/{venue_id}' + Paths.venue.contracts, response_model=List[ContractOut])
# async def fetch_related_contracts(venue_id: uuid.UUID = Path(...), Authorize: AuthJWT = Depends()):
#     Authorize.jwt_required()
#     current_user_id = uuid.UUID(Authorize.get_jwt_subject())
#     user = await get_user_from_db(current_user_id)
#     try:
#         if not (venue := await Venue.objects.select_related('holding_contracts').get(id=venue_id, user=user, is_active=True)):
#             raise VenueDoesNotExistException()
#         contracts = await Contract.objects.select_related(["contact", "venue", "invoices"]).filter(venue=venue).all()
#         return contracts
#     except Exception as err:
#         logger.error(err)
#         raise HttpException(message='failed to fetch related contracts.', detail=str(err))
