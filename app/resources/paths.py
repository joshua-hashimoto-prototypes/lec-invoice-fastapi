from pydantic import BaseModel

from .constants import BASE_API_URL


class PathsBaseModel(BaseModel):
    base: str

    @property
    def test_url(self):
        return f'{BASE_API_URL}/{self.base}'


class AuthPaths(PathsBaseModel):
    base = "auth"
    signup = "/register"
    login = "/login"
    refresh = "/refresh"
    logout = "/logout"
    profile = "/profile"


class ContactPaths(PathsBaseModel):
    base = "contacts"
    all = "/"
    add = "/add"
    edit = "/edit"
    delete = "/delete"
    contracts = "/contracts"


class VenuePaths(PathsBaseModel):
    base = "venues"
    all = "/"
    add = "/add"
    edit = "/edit"
    delete = "/delete"
    contracts = "/contracts"


class ContractPaths(PathsBaseModel):
    base = "contracts"
    all = "/"
    add = "/add"
    edit = "/edit"
    delete = "/delete"

class URLPaths(BaseModel):
    auth = AuthPaths()
    contact = ContactPaths()
    venue = VenuePaths()
    contract = ContractPaths()


Paths = URLPaths()
