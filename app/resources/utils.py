import glob
from typing import List
import random
import string

from humps import camelize


def get_models_file_path() -> List[str]:
    return glob.glob('**/models.py', recursive=True)


def format_path(path: str) -> str:
    return path.rstrip('.py').replace('/', '.')


def get_models_path_list() -> List[str]:
    match_paths = get_models_file_path()
    mapped_paths = map(format_path, match_paths)
    return list(mapped_paths)


def to_camel_case(value: str) -> str:
    return camelize(value)


def random_string() -> str:
    length = random.randrange(10, 25)
    rand_str = ''.join(random.choices(string.ascii_letters + string.digits, k=length))
    return rand_str
