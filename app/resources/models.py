import uuid
from datetime import datetime

import ormar

from app.configs.database import metadata, database


class CoreModel(ormar.Model):
    pk: int = ormar.Integer(primary_key=True)
    id: uuid.UUID = ormar.UUID(default=uuid.uuid4)
    created_at: datetime = ormar.DateTime(default=datetime.now)
    updated_at: datetime = ormar.DateTime(default=datetime.now)
    is_active: bool = ormar.Boolean(default=True)

    class Meta:
        abstract = True
        metadata = metadata
        database = database
