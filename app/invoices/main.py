from fastapi import FastAPI

from .routers import setup_invoices_routers


def setup_invoices(app: FastAPI):
    setup_invoices_routers(app)
