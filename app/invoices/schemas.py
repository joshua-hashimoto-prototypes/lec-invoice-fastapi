from typing import Optional

from .models import Invoice
from app.resources.utils import to_camel_case


InvoiceBaseIn = Invoice.get_pydantic(include={"content", "price", "people", "description", "is_main"})
InvoiceOut = Invoice.get_pydantic(include={"id", "content", "price", "people", "description", "is_main"})


class InvoiceIn(InvoiceBaseIn):

    class Config:
        alias_generator = to_camel_case


class InvoiceEdit(InvoiceIn):
    id: Optional[str] = None
