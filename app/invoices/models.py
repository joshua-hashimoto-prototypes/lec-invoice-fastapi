from typing import Optional

import ormar

from app.users.models import User
from app.resources.models import CoreModel


class Invoice(CoreModel):
    user: Optional[User] = ormar.ForeignKey(User, related_name='invoices')
    content: str = ormar.String(max_length=120)
    price: int = ormar.Integer(default=0)
    people: int = ormar.Integer(default=0)
    description: str = ormar.Text(nullable=True)
    is_main: bool = ormar.Boolean(default=False)

    class Meta(ormar.ModelMeta):
        tablename = "invoices"
