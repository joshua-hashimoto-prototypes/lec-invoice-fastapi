from fastapi import FastAPI, APIRouter

from app.resources.constants import BASE_API_URL


app_name = 'invoices'

router = APIRouter()


def setup_invoices_routers(app: FastAPI):
    app.include_router(
        router,
        prefix=f'/{BASE_API_URL}/{app_name}',
        tags=[app_name],
    )
