from fastapi import FastAPI
from fastapi_jwt_auth import AuthJWT

from .configs.settings import settings
from .configs.fastapi_jwt_auth import setup_jwt_auth_configs
from .configs.middlewares import setup_middleware
from .configs.database import setup_database
from .resources.constants import BASE_API_URL
from .resources.exception_handlers import setup_exception_handlers
from .users.main import setup_auth
from .contacts.main import setup_contacts
from .venues.main import setup_venues
from .invoices.main import setup_invoices
from .contracts.main import setup_contracts


@AuthJWT.load_config
def get_config():
    return settings


app = FastAPI(
    title='LEC Invoice Convenience Service',
    description='REST API for LEC invoice service',
    version='1.0.0',
    docs_url=f'/{BASE_API_URL}/docs'
)


# configs
setup_middleware(app)
setup_database(app)
setup_jwt_auth_configs(app)
setup_exception_handlers(app)

# setup "apps"
setup_auth(app)
setup_contacts(app)
setup_venues(app)
setup_invoices(app)
setup_contracts(app)

# uvicorn app.main:app --reload
