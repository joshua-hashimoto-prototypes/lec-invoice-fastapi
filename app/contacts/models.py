from typing import Optional

import ormar

from app.users.models import User
from app.resources.models import CoreModel


class Contact(CoreModel):
    user: Optional[User] = ormar.ForeignKey(User, related_name='contacts')
    name: str = ormar.String(max_length=50)
    description: str = ormar.Text(nullable=True)

    class Meta(ormar.ModelMeta):
        tablename = "contacts"
