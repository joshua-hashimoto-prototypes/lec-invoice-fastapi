
from .models import Contact

ContactIn = Contact.get_pydantic(include={"name", "description"})
ContactOut = Contact.get_pydantic(include={"id", "name", "description"})
