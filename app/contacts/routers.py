
from typing import List
import uuid

from fastapi import FastAPI, APIRouter, Depends, Body, Path, status
from fastapi.logger import logger
from fastapi_jwt_auth import AuthJWT

from .exceptions import ContactDoesNotExistException
from .models import Contact
from .schemas import ContactIn, ContactOut
from app.users.services import get_user_from_db
from app.resources.constants import BASE_API_URL
from app.resources.exception_handlers import HttpException
from app.resources.paths import Paths


app_name = Paths.contact.base

router = APIRouter()


def setup_contacts_routers(app: FastAPI):
    app.include_router(
        router,
        prefix=f'/{BASE_API_URL}/{app_name}',
        tags=[app_name],
    )


@router.get(Paths.contact.all, response_model=List[ContactOut])
async def get_all_contacts(Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        contacts = await Contact.objects.filter(user=user, is_active=True).all()
        return contacts
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to fetch related data.', detail=str(err))


@router.post(Paths.contact.add, status_code=status.HTTP_201_CREATED, response_model=ContactOut)
async def create_contact(contact_in: ContactIn = Body(...),
                         Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        contact = await Contact.objects.create(user=user, **contact_in.dict())
        return contact
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to save data.', detail=str(err))


@router.get('/{contact_id}', response_model=ContactOut)
async def fetch_contact(contact_id: uuid.UUID = Path(...), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        if not (contact := await Contact.objects.get(id=contact_id, user=user)):
            raise ContactDoesNotExistException()
        return contact
    except Exception as err:
        logger.error(err)
        raise HttpException(message="failed to fetch data", detail=str(err))


@router.put('/{contact_id}' + Paths.contact.edit, response_model=ContactOut)
async def update_contact(contact_id: uuid.UUID = Path(...),
                         contact_in: ContactIn = Body(...),
                         Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        if not (contact := await Contact.objects.get(id=contact_id, user=user)):
            raise ContactDoesNotExistException()
        await contact.update(**contact_in.dict())
        return contact
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to update data.', detail=str(err))


@router.delete('/{contact_id}' + Paths.contact.delete, status_code=status.HTTP_204_NO_CONTENT)
async def delete_contact(contact_id: uuid.UUID = Path(...), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        if not (contact := await Contact.objects.get(id=contact_id, user=user)):
            raise ContactDoesNotExistException()
        await contact.update(is_active=False)
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to delete data.', detail=str(err))
