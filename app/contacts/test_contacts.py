
import random
import json

from fastapi import status
from fastapi.testclient import TestClient
import pytest

from .models import Contact
from .schemas import ContactOut
from app.resources.utils import random_string
from app.resources.paths import Paths


@pytest.fixture(scope="module")
def user(rand_user):
    return rand_user


@pytest.fixture
async def rand_contact(user, db):
    contacts = await db(Contact.objects.filter(user=user, is_active=True).all())
    if len(contacts) == 0:
        return await db(Contact.objects.create(user=user, name="filler contact", description=""))
    return random.choice(contacts)


def test_api_can_add_contacts(client: TestClient, login, logout, user):
    with client as client:
        login(user)
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        context = {
            'name': random_string(),
            'description': random_string(),
        }
        url = Paths.contact.test_url + Paths.contact.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_201_CREATED
        contact = json.loads(response.text)
        assert contact['id'] is not None
        assert contact['name'] == context['name']
        assert contact['description'] == context['description']
    logout()


def test_api_cannot_add_contact(client: TestClient):
    with client as client:
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None)
        }
        context = {
            'name': random_string(),
            'description': random_string(),
        }
        url = Paths.contact.test_url + Paths.contact.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_fetch_user_related_contacts(client: TestClient, login, logout, user):
    with client as client:
        login(user)
        url = Paths.contact.test_url + Paths.contact.all
        response = client.get(url)
        assert response.status_code == status.HTTP_200_OK
    logout()


def test_api_cannot_fetch_user_related_contacts(client: TestClient):
    with client as client:
        url = Paths.contact.test_url + Paths.contact.all
        response = client.get(url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_fetch_contact_detail(client: TestClient, login, logout, user, rand_contact):
    with client as client:
        login(user)
        url = Paths.contact.test_url + "/" + str(rand_contact.id)
        response = client.get(url)
        assert response.status_code == status.HTTP_200_OK
        response_data = json.loads(response.text)
        response_contact = ContactOut(**response_data)
        assert response_contact.id == rand_contact.id
    logout()


def test_api_cannot_fetch_contact_detail(client: TestClient, rand_contact):
    with client as client:
        url = Paths.contact.test_url + "/" + str(rand_contact.id)
        response = client.get(url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_edit_contact(client: TestClient, login, logout, user, rand_contact):
    # NOTE(joshua-hashimoto): withの中ではasyncの処理は実行できない
    with client as client:
        login(user)
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        context = {
            'name': 'editted name',
            'description': 'editted description',
        }
        url = Paths.contact.test_url + '/' + str(rand_contact.id) + Paths.contact.edit
        response = client.put(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_200_OK
        response_data = json.loads(response.text)
        response_contact = ContactOut(**response_data)
        assert response_contact.id == rand_contact.id
        assert response_contact.name == context['name']
        assert response_contact.description == context['description']
    logout()


def test_api_cannot_edit_contact(client: TestClient, rand_contact):
    # NOTE(joshua-hashimoto): withの中ではasyncの処理は実行できない
    with client as client:
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        context = {
            'name': 'editted name',
            'description': 'editted description',
        }
        url = Paths.contact.test_url + '/' + str(rand_contact.id) + Paths.contact.edit
        response = client.put(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    

@pytest.mark.asyncio
async def test_api_can_delete_contact(client: TestClient, db, login, logout, user, rand_contact):
    user_contacts = await db(Contact.objects.filter(user=user, is_active=True).all())
    with client as client:
        login(user)
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        url = Paths.contact.test_url + "/" + str(rand_contact.id) + Paths.contact.delete
        response = client.delete(url, headers=headers)
        assert response.status_code == status.HTTP_204_NO_CONTENT
    updated_user_contacts = await db(Contact.objects.filter(user=user, is_active=True).all())
    assert len(user_contacts) != len(updated_user_contacts)
    logout()
    

@pytest.mark.asyncio
async def test_api_cannot_delete_contact(client: TestClient, rand_contact):
    with client as client:
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None),
        }
        url = Paths.contact.test_url + "/" + str(rand_contact.id) + Paths.contact.delete
        response = client.delete(url, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


# def test_api_can_fetch_contracts_related_to_contact(client: TestClient, event_loop: asyncio.AbstractEventLoop):
#     user_login(client, event_loop)

#     async def get_contact():
#         contacts = await Contact.filter(is_active=True)
#         return contacts[0]

#     contact = event_loop.run_until_complete(get_contact())
#     uri = f'/{PREFIX_URL}/{contact.id}/contracts'
#     response = client.get(uri)
#     assert response.status_code == status.HTTP_200_OK
#     data = json.loads(response.text)
#     decoded_contracts = data.get('contracts', None)
#     assert isinstance(decoded_contracts, list)
#     user_logout(client)


# def test_api_cannot_fetch_contracts_related_to_contact(client: TestClient, event_loop: asyncio.AbstractEventLoop):
#     async def get_contact():
#         contacts = await Contact.filter(is_active=True)
#         return contacts[0]

#     contact = event_loop.run_until_complete(get_contact())
#     uri = f'/{PREFIX_URL}/{contact.id}/contracts'
#     response = client.get(uri)
#     assert response.status_code == status.HTTP_401_UNAUTHORIZED
