import random

from fastapi import status
from fastapi.responses import Response
from fastapi.testclient import TestClient
import pytest

from .models import User
from app.resources.constants import BASE_API_URL
from app.resources.paths import Paths


@pytest.mark.asyncio
async def test_user_registration(client: TestClient, db, logout):
    users = await db(User.objects.all())
    user_count = len(users)
    with client as client:
        user_credentials = {
            'email': f'example{user_count}@example.com',
            'password': 'Abcd1234',
            'password_confirm': "Abcd1234",
        }
        url = Paths.auth.test_url + Paths.auth.signup
        response = client.post(url, json=user_credentials)
        assert response.status_code == 201
        access_token_cookie = response.cookies.get('access_token_cookie', None)
        assert access_token_cookie is not None
        refresh_token_cookie = response.cookies.get('refresh_token_cookie', None)
        assert refresh_token_cookie is not None
        csrf_access_token = response.cookies.get('csrf_access_token', None)
        assert csrf_access_token is not None
        csrf_refresh_token = response.cookies.get('csrf_refresh_token', None)
        assert csrf_refresh_token is not None
    users = await db(User.objects.all())
    assert len(users) == user_count + 1
    logout()


@pytest.mark.asyncio
async def test_user_login(client: TestClient, rand_user, logout):
    with client as client:
        user_credentials = {
            'email': rand_user.email,
            'password': 'Abcd1234',
        }
        url = Paths.auth.test_url + Paths.auth.login
        response = client.post(url, json=user_credentials)
        assert response.status_code == 200
        access_token_cookie = response.cookies.get('access_token_cookie', None)
        assert access_token_cookie is not None
        refresh_token_cookie = response.cookies.get('refresh_token_cookie', None)
        assert refresh_token_cookie is not None
        csrf_access_token = response.cookies.get('csrf_access_token', None)
        assert csrf_access_token is not None
        csrf_refresh_token = response.cookies.get('csrf_refresh_token', None)
        assert csrf_refresh_token is not None
    logout()


def test_user_profile(client: TestClient, login, logout, rand_user):
    with client as client:
        login_response = login(rand_user)
        url = Paths.auth.test_url + Paths.auth.profile
        response = client.get(url)
        assert response.status_code == 200
    logout()


def test_user_cannot_fetch_profile(client: TestClient):
    uri = Paths.auth.test_url + Paths.auth.profile
    response = client.get(uri)
    assert response.status_code == 401


def test_api_can_refresh_jwt(client: TestClient, login, logout, rand_user: User):
    with client as client:
        login(rand_user)
        original_access_token_cookie = client.cookies.get('access_token_cookie', None)
        original_refresh_token_cookie = client.cookies.get('refresh_token_cookie', None)

        csrf_refresh_token = client.cookies.get('csrf_refresh_token', None)
        uri = Paths.auth.test_url + Paths.auth.refresh
        headers = {
            'X-CSRF-REFRESH-TOKEN': csrf_refresh_token
        }

        response = client.post(uri, headers=headers)
        assert response.status_code == status.HTTP_200_OK
        refreshed_access_token_cookie = response.cookies.get('access_token_cookie', None)
        refreshed_refresh_token_cookie = response.cookies.get('refresh_token_cookie', None)
        assert original_access_token_cookie != refreshed_access_token_cookie
        assert original_refresh_token_cookie != refreshed_refresh_token_cookie

        profile_url = Paths.auth.test_url + Paths.auth.profile
        freshed_test_response = client.get(profile_url)
        assert freshed_test_response.status_code == status.HTTP_200_OK
    logout()


def test_api_cannot_refresh_jwt_for_incorrect_csrf(client: TestClient, login, logout, rand_user: User):
    with client as client:
        login(rand_user)
        url = Paths.auth.test_url + Paths.auth.refresh
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_refresh_token')
        }
        response = client.post(url, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    logout()


def test_api_cannot_refresh_jwt(client: TestClient):
    with client as client:
        url = Paths.auth.test_url + Paths.auth.refresh
        response = client.post(url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_logout_user(client: TestClient, login, rand_user: User):
    with client as client:
        login(rand_user)
        url = Paths.auth.test_url + Paths.auth.logout
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None)
        }
        response = client.post(url, headers=headers)
        assert not response.cookies


def test_api_cannot_logout_user(client: TestClient):
    with client as client:
        url = Paths.auth.test_url + Paths.auth.logout
        headers = {
            'X-CSRF-TOKEN': client.cookies.get('csrf_access_token', None)
        }
        response = client.post(url, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
