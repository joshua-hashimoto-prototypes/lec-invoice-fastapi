from typing import Any, Dict, Optional

from fastapi import HTTPException, status


class EmailEmptyException(HTTPException):
    
    def __init__(self, headers: Optional[Dict[str, Any]] = None) -> None:
        status_code = status.HTTP_400_BAD_REQUEST
        detail = "Email cannot be emptry."
        super().__init__(status_code, detail=detail, headers=headers)


class EmailInvalidException(HTTPException):

    def __init__(self, headers: Optional[Dict[str, Any]] = None) -> None:
        status_code = status.HTTP_400_BAD_REQUEST
        detail = "Invalid email address."
        super().__init__(status_code, detail=detail, headers=headers)


class PasswordEmptyException(HTTPException):

    def __init__(self, headers: Optional[Dict[str, Any]] = None) -> None:
        status_code = status.HTTP_400_BAD_REQUEST
        detail = "Password cannot be empty."
        super().__init__(status_code, detail=detail, headers=headers)


class PasswordInvalidException(HTTPException):

    def __init__(self, headers: Optional[Dict[str, Any]] = None) -> None:
        status_code = status.HTTP_400_BAD_REQUEST
        detail = "Password must be more then 8 charactors and contain small letters, capital letters and numbers."
        super().__init__(status_code, detail=detail, headers=headers)


class PasswordNotMatchException(HTTPException):

    def __init__(self, headers: Optional[Dict[str, Any]] = None) -> None:
        status_code = status.HTTP_400_BAD_REQUEST
        detail = "Password does not match."
        super().__init__(status_code, detail=detail, headers=headers)


class InvalidCredentialsException(HTTPException):

    def __init__(self, headers: Optional[Dict[str, Any]] = None) -> None:
        status_code = status.HTTP_401_UNAUTHORIZED
        detail = "Invalid email or password."
        super().__init__(status_code, detail=detail, headers=headers)


class UserDoesNotExistException(HTTPException):

    def __init__(self, headers: Optional[Dict[str, Any]] = None) -> None:
        status_code = status.HTTP_400_BAD_REQUEST
        detail = "User does not exist."
        super().__init__(status_code, detail=detail, headers=headers)
