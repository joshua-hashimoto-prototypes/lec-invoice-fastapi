import uuid

from fastapi.logger import logger

from .models import User
from app.resources.exception_handlers import HttpException


async def get_user_from_db(id: uuid.UUID):
    try:
        return await User.objects.get(id=id, is_active=True)
    except Exception as err:
        logger.error(err)
        raise HttpException(message="Invalid user credentials", detail="User id is invalid.")
