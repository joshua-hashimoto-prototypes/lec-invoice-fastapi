import os
import uuid

from fastapi import FastAPI, APIRouter, Depends, HTTPException, status, Body
from fastapi.logger import logger
from fastapi_jwt_auth import AuthJWT
from passlib.hash import bcrypt

from app.users.exceptions import InvalidCredentialsException

from .models import User
from .schemas import UserLoginIn, UserRegisterIn, UserProfileResponse
from app.resources.constants import BASE_API_URL
from app.resources.exception_handlers import HttpException
from app.resources.paths import Paths


SECRET_KEY = os.environ.get('SECRET_KEY')

app_name = Paths.auth.base

router = APIRouter()


def setup_auth_routers(app: FastAPI):
    app.include_router(
        router,
        prefix=f'/{BASE_API_URL}/{app_name}',
        tags=[app_name],
    )


async def authenticate_user(*, email: str, password: str) -> User:
    try:
        user = await User.objects.get(email=email, is_active=True)
        if not user or not user.verify_password(password):
            return False
        return user
    except Exception as err:
        logger.error(err)
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid credentials")


async def create_user(username: str, email: str, password: str, *args, **kwargs) -> User:
    if not username:
        username = email[:email.index('@')]
    password_hash = bcrypt.hash(password)
    # create user
    user = await User.objects.create(
        username=username,
        email=email,
        password_hash=password_hash
    )
    return user


@router.post(Paths.auth.signup, status_code=status.HTTP_201_CREATED)
async def register_user(user_in: UserRegisterIn = Body(...), Authorize: AuthJWT = Depends()):
    """
    return access token if the registration is successful
    """
    try:
        # create user
        user = await create_user(**user_in.dict())
        user_id = str(user.id)
        # create tokens
        access_token = Authorize.create_access_token(subject=user_id)
        refresh_token = Authorize.create_refresh_token(subject=user_id)
        # set cookies
        Authorize.set_access_cookies(access_token)
        Authorize.set_refresh_cookies(refresh_token)
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to register.', detail=str(err))
    return {'message': 'Successfully register'}


@router.post(Paths.auth.login)
async def login_user(user_in: UserLoginIn = Body(...), Authorize: AuthJWT = Depends()):
    """
    pass email address to form data 'username'
    """
    try:
        user = await authenticate_user(email=user_in.email, password=user_in.password)
        if not user:
            raise InvalidCredentialsException()
        user_id = str(user.id)
        # create tokens
        access_token = Authorize.create_access_token(subject=user_id)
        refresh_token = Authorize.create_refresh_token(subject=user_id)
        # set cookies
        Authorize.set_access_cookies(access_token)
        Authorize.set_refresh_cookies(refresh_token)
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to login user.', detail=str(err))
    return {'message': 'Successfully login'}


@router.post(Paths.auth.refresh)
async def refresh_user(Authorize: AuthJWT = Depends()):
    Authorize.jwt_refresh_token_required()
    current_user_id = Authorize.get_jwt_subject()
    # create tokens
    access_token = Authorize.create_access_token(subject=current_user_id)
    refresh_token = Authorize.create_refresh_token(subject=current_user_id)
    # set cookies
    Authorize.set_access_cookies(access_token)
    Authorize.set_refresh_cookies(refresh_token)
    return {'message': 'Successfully refreshed token'}


@router.post(Paths.auth.logout)
async def logout_user(Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()

    Authorize.unset_jwt_cookies()
    return {'message': 'Successfully logout'}


@router.get(Paths.auth.profile, summary="login required", response_model=UserProfileResponse)
async def fetch_user_profile(Authorize: AuthJWT = Depends()):
    """
    **Login Required**
    """
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    try:
        user = await User.objects.get(id=current_user_id, is_active=True)
        if user is None:
            raise HttpException(message='failed to fetch related data.', detail="user does not exist.")
        return user
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to fetch related data.', detail=str(err))
