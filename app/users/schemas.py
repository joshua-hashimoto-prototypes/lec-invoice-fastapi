import re
from typing import Optional

from pydantic import BaseModel, validator

from .exceptions import (
    EmailEmptyException,
    EmailInvalidException,
    PasswordEmptyException,
    PasswordInvalidException,
    PasswordNotMatchException,
)
from .models import User
from app.resources.constants import EMAIL_REGEX, PASSWORD_REGEX


class UserBaseIn(BaseModel):
    email: str
    password: str

    @validator('email')
    def email_validator(cls, value):
        if not value:
            raise EmailEmptyException()
        if not re.match(EMAIL_REGEX, value):
            raise EmailInvalidException()
        return value

    @validator('password')
    def password_validator(cls, value):
        if not value:
            raise PasswordEmptyException()
        if not re.match(PASSWORD_REGEX, value):
            raise PasswordInvalidException()
        return value


class UserRegisterIn(UserBaseIn):
    username: Optional[str] = None
    password_confirm: str

    @validator('password_confirm')
    def passwords_match_validator(cls, value, values, **kwargs):
        if 'password' in values and value != values['password']:
            raise PasswordNotMatchException
        return value


class UserLoginIn(UserBaseIn):
    pass


UserProfileResponse = User.get_pydantic(exclude={"contacts", })
