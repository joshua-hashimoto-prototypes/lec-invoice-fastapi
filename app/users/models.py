import ormar
from passlib.hash import bcrypt

from app.resources.models import CoreModel


class User(CoreModel):
    username: str = ormar.String(max_length=50, unique=True)
    email: str = ormar.String(max_length=60, unique=True)
    password_hash: str = ormar.String(max_length=128)

    def verify_password(self, password):
        return bcrypt.verify(password, self.password_hash)

    class Meta(ormar.ModelMeta):
        tablename = "users"
