from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from fastapi_jwt_auth.exceptions import AuthJWTException


def setup_jwt_auth_configs(app: FastAPI):
    # custom error handling
    @app.exception_handler(AuthJWTException)
    def authjwt_expection_handler(request: Request, exc: AuthJWTException):
        return JSONResponse(
            status_code=exc.status_code,
            content={"detail": exc.message}
        )
