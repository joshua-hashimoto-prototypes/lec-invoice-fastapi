import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

# TODO: add production domain
origins = os.environ.get('CORS_ORIGINS').split(',')


def setup_middleware(app: FastAPI):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*'],
    )
