import os

import databases
from fastapi import FastAPI
import sqlalchemy

from app.configs.settings import settings
from app.resources.utils import get_models_path_list

models_path_list = get_models_path_list()

TEST_DB_URL = f"sqlite:///{str(settings.BASE_DIR)}/db/test_db.sqlite"


def get_db_uri(*, user, password, host, db, port="5432"):
    if settings.environment == "test":
        return TEST_DB_URL
    return f'postgresql://{user}:{password}@{host}:{port}/{db}'


DB_URL = get_db_uri(
    user=os.environ.get('POSTGRES_USER'),
    password=os.environ.get('POSTGRES_PASSWORD'),
    host='db',  # docker-composeのservice名
    db=os.environ.get('POSTGRES_DB'),
)
database = databases.Database(DB_URL)
metadata = sqlalchemy.MetaData()


def setup_database(app: FastAPI):
    app.state.database = database

    @app.on_event("startup")
    async def startup() -> None:
        database_ = app.state.database
        if not database_.is_connected:
            await database_.connect()

    @app.on_event("shutdown")
    async def shutdown() -> None:
        database_ = app.state.database
        if database_.is_connected:
            await database_.disconnect()
