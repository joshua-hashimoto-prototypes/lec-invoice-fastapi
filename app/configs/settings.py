import os
from datetime import timedelta
from pathlib import Path

from pydantic import BaseSettings


BASE_DIR = Path(__file__).resolve(strict=True).parent.parent.parent

SECRET_KEY = os.environ.get('SECRET_KEY')
DEBUG = int(os.environ.get('DEBUG'))
ENVIRONMENT = os.environ.get("ENVIRONMENT")


class Settings(BaseSettings):
    secret_key: str = SECRET_KEY
    environment: str = ENVIRONMENT
    authjwt_secret_key: str = SECRET_KEY  # for fastapi_jwt_auth
    authjwt_token_location: set = {'cookies'}  # for fastapi_jwt_auth
    authjwt_cookie_secure: bool = not DEBUG  # for fastapi_jwt_auth
    authjwt_cookie_csrf_protect: bool = True
    authjwt_cookie_samesite: str = 'lax'  # for fastapi_jwt_auth
    authjwt_access_token_expires: timedelta = timedelta(days=30)
    authjwt_refresh_token_expires: timedelta = timedelta(days=90)
    authjwt_refresh_csrf_header_name = 'X-CSRF-REFRESH-TOKEN'
    BASE_DIR: Path = BASE_DIR


settings = Settings()
