from fastapi import FastAPI

from .routers import setup_contracts_routers


def setup_contracts(app: FastAPI):
    setup_contracts_routers(app)
