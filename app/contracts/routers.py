from typing import List
import uuid

from fastapi import FastAPI, APIRouter, Depends, Body, status, Path
from fastapi.logger import logger
from fastapi_jwt_auth import AuthJWT
from ormar.exceptions import NoMatch, ModelError

from .models import Contract
from .schemas import ContractIn, ContractOut, ContractEdit
from app.users.services import get_user_from_db
from app.contacts.models import Contact
from app.resources.constants import BASE_API_URL
from app.resources.paths import Paths
from app.resources.exception_handlers import HttpException
from app.invoices.models import Invoice
from app.invoices.schemas import InvoiceEdit
from app.venues.models import Venue


app_name = Paths.contract.base

router = APIRouter()


def setup_contracts_routers(app: FastAPI):
    app.include_router(
        router,
        prefix=f'/{BASE_API_URL}/{app_name}',
        tags=[app_name],
    )


@router.get(Paths.contract.all, response_model=List[ContractOut])
async def get_all_contracts(Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        contracts = await Contract.objects.select_related(["contact", "venue", "invoices"]).filter(user=user, is_active=True).all()
        return contracts
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to fetch related data.', detail=str(err))


@router.post(Paths.contract.add, status_code=status.HTTP_201_CREATED, response_model=ContractOut)
async def create_contract(contract_in: ContractIn = Body(...), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        contact = await Contact.objects.get(id=uuid.UUID(contract_in.contact_id))
        venue = await Venue.objects.get(id=uuid.UUID(contract_in.venue_id))
        contract_in_dict = contract_in.dict(exclude={"contact_id", "venue_id"})
        contract = await Contract.objects.create(user=user, contact=contact, venue=venue, **contract_in_dict)
        # NOTE(joshua-hashimoto): pkを渡すのであれば、以下のメソッドでも問題ない。しかし実際にフロントから来る値にはpkは含まれないので、手動で全てやる必要がある
        # await contract.save_related(follow=True, save_all=True)
        invoices = [await Invoice.objects.get_or_create(user=user, is_active=True, **invoice.dict()) for invoice in contract_in.invoices]
        for invoice in invoices:
            await contract.invoices.add(invoice)
        return contract
    except ModelError as model_err:
        logger.error(model_err)
        raise HttpException(message='failed to save data.', detail="ModelError")
    except AssertionError as assert_err:
        logger.error(assert_err)
        raise HttpException(message='failed to save data.', detail="AssertionError")
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to save data.', detail=str(err))


@router.get("/{contract_id}", response_model=ContractOut)
async def fetch_contract_detail(contract_id: uuid.UUID = Path(...), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        contract = await Contract.objects.select_related(["contact", "venue", "invoices"]).get(id=contract_id, user=user, is_active=True)
        return contract
    except NoMatch as no_match_err:
        logger.error(no_match_err)
        raise HttpException(message='failed to save data.', detail=str(no_match_err))
    except Exception as err:
        logger.error(err)
        raise HttpException(message='failed to save data.', detail=str(err))


@router.put("/{contract_id}" + Paths.contract.edit, response_model=ContractOut)
async def edit_contract(contract_id: uuid.UUID = Path(...), contract_in: ContractEdit = Body(...), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        contact = await Contact.objects.get(id=uuid.UUID(contract_in.contact_id))
        venue = await Venue.objects.get(id=uuid.UUID(contract_in.venue_id))
        contract_in_dict = contract_in.dict(exclude={"contact_id", "venue_id"})
        contract = await Contract.objects.get(id=contract_id)
        await contract.update(contact=contact, venue=venue, **contract_in_dict)

        # NOTE(joshua-hashimoto): 一度データを全て削除する
        await contract.invoices.clear()
        # NOTE(joshua-hashimoto): invoicesにはidがある可能性があるが、pkではないため手動で処理を変える
        #                         pkがあれば、update_or_createなど、他の便利メソッドが使える
        for invoice in contract_in.invoices:
            invoice_dict = invoice.dict(exclude={"id"})
            if invoice.id:
                new_invoice = await Invoice.objects.get(id=uuid.UUID(invoice.id))
                await new_invoice.update(**invoice_dict)
            else:
                new_invoice = await Invoice.objects.create(**invoice_dict)
            await contract.invoices.add(new_invoice)
        return contract
    except Exception as err:
        logger.error(err)
        raise HttpException(message="failed to update data.", detail=str(err))


@router.delete("/{contract_id}" + Paths.contract.delete, status_code=status.HTTP_204_NO_CONTENT)
async def delete_contract(contract_id: uuid.UUID = Path(...), Authorize: AuthJWT = Depends()):
    Authorize.jwt_required()
    current_user_id = uuid.UUID(Authorize.get_jwt_subject())
    user = await get_user_from_db(current_user_id)
    try:
        contract = await Contract.objects.get(id=contract_id, user=user, is_active=True)
        await contract.update(is_active=False)
        return {"message": "Successfully deleted contract."}
    except Exception as err:
        logger.error(err)
        raise HttpException(message="failed to update data.", detail=str(err))
