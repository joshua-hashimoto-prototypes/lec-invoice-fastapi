from datetime import datetime as dt
import json
import random

from fastapi import status
from fastapi.testclient import TestClient
import pytest

from .models import Contract
from .schemas import ContractOut
from app.contacts.models import Contact
from app.invoices.models import Invoice
from app.users.models import User
from app.venues.models import Venue
from app.resources.paths import Paths
from app.resources.utils import random_string


@pytest.fixture(scope="module")
def user(rand_user):
    return rand_user


@pytest.fixture(autouse=True)
def force_logout(logout):
    # NOTE(joshua-hashimoto): 各テスト関数の実行時に自動で必ずlogoutが実行されるようにfixtureを設定
    logout()


@pytest.fixture
async def rand_contact(user, db):
    contacts = await db(Contact.objects.filter(user=user, is_active=True).all())
    if len(contacts) == 0:
        return await db(Contact.objects.create(user=user, name=random_string(), description=random_string()))
    return random.choice(contacts)


@pytest.fixture
async def rand_venue(user, db):
    venues = await db(Venue.objects.filter(user=user, is_active=True).all())
    if len(venues) == 0:
        return await db(Venue.objects.create(user=user, name=random_string(), description=random_string()))
    return random.choice(venues)


@pytest.fixture
async def rand_invoice(user, db):
    invoices = await db(Invoice.objects.filter(user=user, is_active=True).all())
    if len(invoices) == 0:
        return await db(Invoice.objects.create(
            user=user,
            content=random_string(),
            price=random.randrange(1000, 5000, 100),
            people=random.randrange(10, 50, 2),
            description=random_string(),
            is_main=bool(random.getrandbits(1)))
        )
    return random.choice(invoices)


@pytest.fixture
async def rand_contract(user, rand_contact, rand_venue, rand_invoice, db):
    contracts = await db(Contract.objects.filter(user=user, is_active=True).all())
    if len(contracts) == 0:
        contract = await db(Contract.objects.create(
            user=user,
            contact=rand_contact,
            venue=rand_venue,
            holding_at="2022-01-01 01:59:30",
            billing_at="2022-01-01 01:59:30",
            description=random_string()
        ))
        await contract.invoices.add(rand_invoice)
        return contract
    return random.choice(contracts)


def test_api_can_add_contracts(client: TestClient, login, user, rand_contact: Contact, rand_venue: Venue):
    with client as client:
        login(user)
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        context = {
            "contactId": str(rand_contact.id),
            "venueId": str(rand_venue.id),
            "invoices": [
                {
                    "content": random_string(),
                    "price": random.randrange(1200, 2500, 100),
                    "people": random.randrange(10, 50, 2),
                    "description": random_string(),
                    "isMain": False,
                }
            ],
            "holdingAt": "2022-01-01 01:59:3",
            "billingAt": None,
            "description": "",
        }
        url = Paths.contract.test_url + Paths.contract.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_201_CREATED


def test_api_can_add_contracts_with_existing_invoices(client: TestClient, login, user, rand_contact: Contact, rand_venue: Venue, rand_invoice: Invoice):
    with client as client:
        login(user)
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        context = {
            "contactId": str(rand_contact.id),
            "venueId": str(rand_venue.id),
            "invoices": [
                {
                    "content": random_string(),
                    "price": random.randrange(1200, 2500, 100),
                    "people": random.randrange(10, 50, 2),
                    "description": random_string(),
                    "isMain": False,
                },
                rand_invoice.dict(exclude={"pk", "id", "created_at", "updated_at", "user"}),
            ],
            "holdingAt": "2022-01-01 01:59:3",
            "billingAt": None,
            "description": "",
        }
        url = Paths.contract.test_url + Paths.contract.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_201_CREATED


def test_api_can_add_contracts_with_empty_invoices(client: TestClient, login, user, rand_contact: Contact, rand_venue: Venue, rand_invoice: Invoice):
    with client as client:
        login(user)
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        context = {
            "contactId": str(rand_contact.id),
            "venueId": str(rand_venue.id),
            "invoices": [],
            "holdingAt": "2022-01-01 01:59:3",
            "billingAt": None,
            "description": "",
        }
        url = Paths.contract.test_url + Paths.contract.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_201_CREATED


def test_api_cannot_add_contracts_due_to_invalid_credentials(client: TestClient, rand_venue, rand_contact):
    with client as client:
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        context = {
            "contactId": str(rand_contact.id),
            "venueId": str(rand_venue.id),
            "invoices": [
                {
                    "content": random_string(),
                    "price": random.randrange(1200, 2500, 100),
                    "people": random.randrange(10, 50, 2),
                    "description": random_string(),
                    "is_main": False,
                }
            ],
            "holdingAt": dt.now().strftime("%Y-%m-%d %H:%M:%S"),
            "billingAt": None,
            "description": "",
        }
        url = Paths.contract.test_url + Paths.contract.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_cannot_add_contracts_due_to_invalid_contact_id(client: TestClient, login, user, rand_venue):
    with client as client:
        login(user)
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        context = {
            "contactId": "",
            "venueId": str(rand_venue.id),
            "invoices": [
                {
                    "content": random_string(),
                    "price": random.randrange(1200, 2500, 100),
                    "people": random.randrange(10, 50, 2),
                    "description": random_string(),
                    "is_main": False,
                }
            ],
            "holdingAt": dt.now().isoformat(),
            "billingAt": None,
            "description": "",
        }
        url = Paths.contract.test_url + Paths.contract.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_api_cannot_add_contracts_due_to_invalid_venue_id(client: TestClient, login, user, rand_contact):
    with client as client:
        login(user)
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        context = {
            "contactId": str(rand_contact.id),
            "venueId": "",
            "invoices": [
                {
                    "content": random_string(),
                    "price": random.randrange(1200, 2500, 100),
                    "people": random.randrange(10, 50, 2),
                    "description": random_string(),
                    "is_main": False,
                }
            ],
            "holdingAt": dt.now().isoformat(),
            "billingAt": None,
            "description": "",
        }
        url = Paths.contract.test_url + Paths.contract.add
        response = client.post(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_api_can_fetch_user_contracts(client: TestClient, login, user):
    with client as client:
        login(user)
        url = Paths.contract.test_url + Paths.contract.all
        response = client.get(url)
        assert response.status_code == status.HTTP_200_OK


def test_api_cannot_fetch_user_contracts(client: TestClient):
    with client as client:
        url = Paths.contract.test_url + Paths.contract.all
        response = client.get(url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_fetch_user_contract(client: TestClient, login, user, rand_contract: Contract):
    with client as client:
        login(user)
        url = Paths.contract.test_url + "/" + str(rand_contract.id)
        response = client.get(url)
        assert response.status_code == status.HTTP_200_OK
        response_data = json.loads(response.text)
        response_contract = ContractOut(**response_data)
        assert response_contract.id == rand_contract.id


def test_api_cannot_fetch_user_contract(client: TestClient, rand_contract: Contract):
    with client as client:
        url = Paths.contract.test_url + "/" + str(rand_contract.id)
        response = client.get(url)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_edit_contracts(client: TestClient, login, user, rand_contact: Contact, rand_venue: Venue, rand_invoice: Invoice, rand_contract: Contract):
    with client as client:
        login(user)
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        invoice_1 = {
            "content": random_string(),
            "price": random.randrange(1200, 2500, 100),
            "people": random.randrange(10, 50, 2),
            "description": random_string(),
            "isMain": False,
        }
        invoice_2 = {
            "id": str(rand_invoice.id),
            "content": rand_invoice.content + "(edited)",
            "price": rand_invoice.price * 2,
            "people": rand_invoice.people * 3,
            "description": rand_invoice.description,
            "isMain": rand_invoice.is_main,
        }
        context = {
            "contactId": str(rand_contact.id),
            "venueId": str(rand_venue.id),
            "invoices": [
                invoice_1,
                invoice_2,
            ],
            "holdingAt": "2022-01-01 01:59:3",
            "billingAt": None,
            "description": "",
        }
        url = Paths.contract.test_url + "/" + str(rand_contract.id) + Paths.contract.edit
        response = client.put(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_200_OK
        response_data = json.loads(response.text)
        assert response_data["id"] == str(rand_contract.id)
        invoices = response_data["invoices"]
        assert len(invoices) == 2
        assert len(list(filter(lambda x: x["id"] == str(rand_invoice.id), invoices))) == 1


def test_api_can_edit_contracts_without_invoices(client: TestClient, login, user, rand_contact: Contact, rand_venue: Venue, rand_contract: Contract):
    with client as client:
        login(user)
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        context = {
            "contactId": str(rand_contact.id),
            "venueId": str(rand_venue.id),
            "invoices": [],
            "holdingAt": "2022-01-01 01:59:3",
            "billingAt": None,
            "description": "",
        }
        url = Paths.contract.test_url + "/" + str(rand_contract.id) + Paths.contract.edit
        response = client.put(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_200_OK
        response_data = json.loads(response.text)
        assert response_data["id"] == str(rand_contract.id)
        invoices = response_data["invoices"]
        assert len(invoices) == 0


def test_api_cannot_edit_contracts_due_to_invalid_credentials(client: TestClient, rand_contact: Contact, rand_venue: Venue, rand_invoice: Invoice, rand_contract: Contract):
    with client as client:
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        invoice_1 = {
            "content": random_string(),
            "price": random.randrange(1200, 2500, 100),
            "people": random.randrange(10, 50, 2),
            "description": random_string(),
            "isMain": False,
        }
        invoice_2 = {
            "id": str(rand_invoice.id),
            "content": rand_invoice.content + "(edited)",
            "price": rand_invoice.price * 2,
            "people": rand_invoice.people * 3,
            "description": rand_invoice.description,
            "isMain": rand_invoice.is_main,
        }
        context = {
            "contactId": str(rand_contact.id),
            "venueId": str(rand_venue.id),
            "invoices": [
                invoice_1,
                invoice_2,
            ],
            "holdingAt": "2022-01-01 01:59:3",
            "billingAt": None,
            "description": "",
        }
        url = Paths.contract.test_url + "/" + str(rand_contract.id) + Paths.contract.edit
        response = client.put(url, json=context, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_api_can_delete_contract(client: TestClient, user: User, login, rand_contract: Contract):
    with client as client:
        login(user)
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        url = Paths.contract.test_url + "/" + str(rand_contract.id) + Paths.contract.delete
        response = client.delete(url, json={}, headers=headers)
        assert response.status_code == status.HTTP_204_NO_CONTENT


def test_api_cannot_delete_contract_due_to_invalid_credentials(client: TestClient, rand_contract: Contract):
    with client as client:
        headers = {
            "X-CSRF-TOKEN": client.cookies.get("csrf_access_token", None)
        }
        url = Paths.contract.test_url + "/" + str(rand_contract.id) + Paths.contract.delete
        response = client.delete(url, json={}, headers=headers)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
