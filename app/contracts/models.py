from datetime import datetime
from typing import Optional, List

import ormar

from app.users.models import User
from app.contacts.models import Contact
from app.invoices.models import Invoice
from app.venues.models import Venue
from app.resources.models import CoreModel


class Contract(CoreModel):
    user: Optional[User] = ormar.ForeignKey(User, related_name='related_contracts')
    contact: Optional[Contact] = ormar.ForeignKey(Contact, related_name='assigned_contracts')
    venue: Optional[Venue] = ormar.ForeignKey(Venue, related_name='holding_contracts')
    invoices: Optional[List[Invoice]] = ormar.ManyToMany(Invoice, related_name='contracts', nullable=True)
    holding_at: datetime = ormar.DateTime(nullable=True)
    billing_at: datetime = ormar.DateTime(nullable=True)
    description: str = ormar.Text(nullable=True)

    class Meta(ormar.ModelMeta):
        tablename = "contracts"
