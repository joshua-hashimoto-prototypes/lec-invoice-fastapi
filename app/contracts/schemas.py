from datetime import date, datetime
from typing import List, Optional
import uuid

from pydantic import BaseModel

from .models import Contract
from app.contacts.schemas import ContactOut
from app.invoices.schemas import InvoiceIn, InvoiceOut, InvoiceEdit
from app.venues.schemas import VenueOut
from app.resources.utils import to_camel_case


ContractBase = Contract.get_pydantic(include=("id", "holding_at", "billing_at", "description"))


class ContractIn(BaseModel):
    contact_id: str
    venue_id: str
    invoices: List[InvoiceIn]
    holding_at: datetime
    billing_at: Optional[datetime] = None
    description: str

    class Config:
        # allow_population_by_field_name = True
        alias_generator = to_camel_case
        schema_extra = {
            "user": uuid.uuid4(),
            "contact": uuid.uuid4(),
            "venue": uuid.uuid4(),
            "invoices": [
                {
                    "content": "invoice content",
                    "price": "invoice price",
                    "people": "amount of participants",
                    "description": "invoice description. can bbe null",
                    "is_main": "wither the invoice is the main content or not"
                },
            ],
            "holding_at": date.today(),
            "billing_at": date.today(),
        }


class ContractOut(ContractBase):
    contact: ContactOut
    venue: VenueOut
    invoices: List[InvoiceOut]


class ContractEdit(ContractIn):
    invoices: List[InvoiceEdit]
